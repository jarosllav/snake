#pragma once

#include <SFML/Graphics/RenderWindow.hpp>

namespace Context {
	const sf::Vector2u MapSize(15u, 15u);
	const unsigned int TileSize = 32u;
	sf::RenderWindow* Window;
}