#pragma once
#include <iostream>
#include <memory>

#include <SFML/Graphics.hpp>

#include "Context.hpp"
#include "Map.hpp"
#include "Snake.hpp"
#include "Obstacle.hpp"

static float GAME_SPEED = 0.35f;

class Game {
public:
	Game(unsigned int tileSize, const sf::Vector2u& mapSize)
		: map(tileSize, mapSize, sf::Color(155, 155, 155, 255)),
		snake(tileSize, sf::Color(50, 200, 50), sf::Color(10, 250, 10)),
		obstacle(tileSize, sf::Color::Red)
	{
		this->mapSize = mapSize;
		this->tileSize = tileSize;

		window = std::make_unique<sf::RenderWindow>(sf::VideoMode(mapSize.x * tileSize, mapSize.y * tileSize, 32u), "Snake");
		window->setFramerateLimit(60u);
	}

	void run() {
		generateObstacle();
		snake.reset();
		while (window->isOpen()) {
			this->checkEvents();
			this->update();
			this->draw();
		}
	}

	void checkEvents() {
		while (window->pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window->close();
			}
			else if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Space) {
					snake.grow();
				}
			}
		}
	}

	void update() {
		snake.update();
		if (clock.getElapsedTime().asSeconds() >= GAME_SPEED) {
			directions direction = snake.getTriggerDirection();
			snake.setDirection(direction);

			sf::Vector2i movement(0.f, 0.f);
			if (direction == directions::right)
				movement.x += 1;
			else if (direction == directions::left)
				movement.x -= 1;
			else if (direction == directions::up)
				movement.y -= 1;
			else if (direction == directions::down)
				movement.y += 1;

			auto body = snake.getBody();
			sf::Vector2i newPosition = movement + snake.getHeadPosition();

			for (auto i = 1u; i < body.size(); ++i) {
				if (newPosition == body[i]) {
					gameOver();
				}
			}
			
			if (newPosition == obstacle.getPosition()) {
				snake.grow();
				generateObstacle();
			}

			if (newPosition.x < 0 || newPosition.x >= Context::MapSize.x) {
				gameOver();
				movement.x = 0;
			}
			if (newPosition.y < 0 || newPosition.y >= Context::MapSize.y) {
				gameOver();
				movement.y = 0;
			}

			snake.move(movement);
			clock.restart();
		}
	}

	void draw() {
		window->clear();
		map.draw(window.get());
		obstacle.draw(window.get());
		snake.draw(window.get());
		window->display();
	}

	void generateObstacle() {
		auto snakeBody = snake.getBody();
		sf::Vector2i position;
		while (true) {
			position.x = rand() % map.getSize().x;
			position.y = rand() % map.getSize().y;

			bool goodPosition = true;
			for (auto element : snakeBody) {
				if (element.x == position.x && element.y == position.y) {
					goodPosition = false;
					break;
				}
			}

			if (goodPosition)
				break;
		}

		obstacle.setPosition(position);
	}

	void gameOver() {
		std::cout << "Game OVER!\n";
		std::cout << "Your score: " << snake.getBody().size() * 5 << "\n";

		snake.reset();
		generateObstacle();
	}

private:
	std::unique_ptr<sf::RenderWindow> window;
	sf::Vector2u mapSize;
	unsigned int tileSize;

	sf::Clock clock;
	sf::Event event;

	Snake snake;
	Map map;
	Obstacle obstacle;
};