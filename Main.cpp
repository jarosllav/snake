#include "Game.hpp"

int main() {
	Game snake(32u, { 15u, 15u });
	snake.run();
	return EXIT_SUCCESS;
}