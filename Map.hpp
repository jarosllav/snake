#pragma once
#include <SFML/Graphics.hpp>

class Map {
public:
	Map(unsigned int tileSize, const sf::Vector2u& mapSize, const sf::Color& color = sf::Color::White) {
		this->tileSize = tileSize;
		this->mapSize = mapSize;

		this->tile.setFillColor(sf::Color::Transparent);
		this->tile.setOutlineColor(color);
		this->tile.setOutlineThickness(.75f);
		this->tile.setSize(sf::Vector2f(tileSize, tileSize));
	}

	void draw(sf::RenderWindow* window) {
		for (int y = 0; y < mapSize.y; ++y) {
			for (int x = 0; x < mapSize.x; ++x) {
				tile.setPosition(sf::Vector2f(x * tileSize, y * tileSize));
				window->draw(tile);
			}
		}
	}

	inline sf::Vector2u getSize() const { return this->mapSize; }

private:
	sf::RectangleShape tile;
	sf::Vector2u mapSize;
	unsigned int tileSize;

};