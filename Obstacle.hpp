#pragma once
#include <SFML/Graphics.hpp>

class Obstacle {
public:
	Obstacle(unsigned int tileSize, const sf::Color& color) {
		this->shape.setSize(sf::Vector2f(tileSize, tileSize));
		this->shape.setFillColor(color);

		this->tileSize = tileSize;
	}

	void setPosition(const sf::Vector2i& position) {
		this->position = position;
		this->shape.setPosition(sf::Vector2f(position.x * tileSize, position.y * tileSize));
	}

	void draw(sf::RenderWindow* window) {
		window->draw(this->shape);
	}

	inline sf::Vector2i getPosition() const { return this->position; }

private:
	unsigned int tileSize;
	sf::RectangleShape shape;
	sf::Vector2i position;
};