#pragma once
#include <vector>

#include <SFML/Graphics.hpp>

enum directions {
	left,
	right,
	up,
	down
};

class Snake {
public:
	Snake(unsigned int tileSize, const sf::Color& headColor, const sf::Color& bodyColor) {
		this->headColor = headColor;
		this->bodyColor = bodyColor;
		this->tileSize = tileSize;
		this->direction = directions::right;
		this->triggerDirection = directions::right;

		shape.setSize(sf::Vector2f(tileSize, tileSize));
		shape.setFillColor(headColor);
	}

	void reset() {
		bodyElements.clear();
		bodyElements.push_back(sf::Vector2i(1, 1));
		direction = directions::right;
		triggerDirection = directions::right;
	}

	void grow() {
		bodyElements.push_back(bodyElements.back());
	}

	void move(const sf::Vector2i& movement) {
		for (int i = bodyElements.size() - 1; i >= 1; --i)
			bodyElements[i] = bodyElements[i - 1];
		this->bodyElements.front() += movement;
	}

	void update() {
		if (sf::Keyboard::isKeyPressed(movementKeys.right) && direction != directions::left)
			triggerDirection = directions::right;
		else if (sf::Keyboard::isKeyPressed(movementKeys.left) && direction != directions::right)
			triggerDirection = directions::left;
		else if (sf::Keyboard::isKeyPressed(movementKeys.down) && direction != directions::up)
			triggerDirection = directions::down;
		else if (sf::Keyboard::isKeyPressed(movementKeys.up) && direction != directions::down)
			triggerDirection = directions::up;
	}

	void draw(sf::RenderWindow* window) {
		this->shape.setFillColor(headColor);
		for (auto& element : this->bodyElements) {
			this->shape.setPosition(tileSize * element.x, tileSize * element.y);
			window->draw(shape);
			this->shape.setFillColor(bodyColor);
		}
	}

	inline void setDirection(directions direction) {
		this->direction = direction;
	}
	
	inline std::vector<sf::Vector2i> getBody() const { return this->bodyElements; }
	inline sf::Vector2i getHeadPosition() const { return this->bodyElements.front(); }
	inline directions getDirection() const { return this->direction; }
	inline directions getTriggerDirection() const { return this->triggerDirection; }
	inline sf::Keyboard::Key getMovementKey(directions dir) const {
		switch (dir) {
			case directions::left:	return this->movementKeys.left;
			case directions::right:	return this->movementKeys.right;
			case directions::up:	return this->movementKeys.up;
			case directions::down:	return this->movementKeys.down;
		}
	}

private:
	struct {
		sf::Keyboard::Key left = sf::Keyboard::A;
		sf::Keyboard::Key right = sf::Keyboard::D;
		sf::Keyboard::Key up = sf::Keyboard::W;
		sf::Keyboard::Key down = sf::Keyboard::S;
	} movementKeys;
	directions direction;
	directions triggerDirection;
	std::vector<sf::Vector2i> bodyElements;

	unsigned int tileSize;
	sf::Color headColor;
	sf::Color bodyColor;
	sf::RectangleShape shape;
};